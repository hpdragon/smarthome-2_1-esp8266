﻿# Smart Home Switch

This is a smart switch project for smart home system, the switch will control 220V electronic in home and is controlled by the server. The switch use ESP8266 as the main mcu for controlling electronic devices and connect to the server.

- MCU: `ESP8266`
- Framework: `ESP8266_RTOS_SDK` (version: 1.3)

![Alt text](image/front.jpg?raw=false)
![Alt text](image/inside.jpg?raw=false)
![Alt text](image/inside_2.jpg?raw=false)

