#include "boss_define.h"
#include "boss.h"

#include "freertos/task.h"
#include "freertos/queue.h"

#include "gpio.h"

/**
 * BOSS task priority
 */
#define BOSS_Priority       0

xTaskHandle BOSS_Init(void)
{
    xTaskHandle boss;

    ConfigLight();

    xTaskCreate( BOSS_Task, "BOSS_Task", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + BOSS_Priority, boss);

    return boss;
}

void BOSS_Task(void *pArg)
{
    xQueueHandle qChecker[3], qTalker[3];
    bool lightStatus[] = {false, false, false};
    uint8 bossMode = BOSS_Mode_Startup, lightData = 0;
    uint8 noButton = NO_BUTTON;
    uint8 checkerData = 0;
    uint8 talkerData = 0xff, talkerOrder = TALKER_Mode_IDLE, talkerTeller = TALKER_Tell_Nothing;

    qChecker[BOSS_qReceive] = xQueueCreate(1, sizeof(uint8));
    qChecker[BOSS_qSend] = xQueueCreate(1, sizeof(uint8));
    qChecker[BOSS_qOrder] = xQueueCreate(1, sizeof(uint8));
    qTalker[BOSS_qReceive] = xQueueCreate(1, sizeof(uint8));
    qTalker[BOSS_qSend] = xQueueCreate(1, sizeof(uint8));
    qTalker[BOSS_qOrder] = xQueueCreate(1, sizeof(uint8));

    TALKER_Init((void *)qTalker);
    CHECKER_Init((void *)qChecker);

    // wifi_status_led_install();
    // GPIO_OUTPUT_SET(2, false);

    printf("Init done\n");

    talkerOrder = TALKER_Mode_IDLE;
    xQueueSend(qTalker[BOSS_qOrder], &talkerOrder, 2);

    while(1)
    {
        switch(bossMode)
        {
        case BOSS_Mode_SetIDLE:
printf("BOSS: Set IDLE\n");
            talkerOrder = TALKER_Mode_IDLE;
            xQueueSend(qTalker[BOSS_qOrder], &talkerOrder, 2);

            bossMode = BOSS_Mode_Startup;
        break;
        case BOSS_Mode_Startup:
            // handle button
            xQueueReceive(qChecker[BOSS_qReceive], &checkerData, 2);
            if( BOSS_ButtonHandler(checkerData, talkerData, lightStatus) == BUTTON_HOLD3 )
            {
                bossMode = BOSS_Mode_GetWifiInfor;
                break;
            }

            xQueueReceive(qTalker[BOSS_qReceive], &talkerTeller, 2);

            // switch to mode talking
            if(talkerTeller == TALKER_Tell_GotIp)
            {
                bossMode = BOSS_Mode_TalkingtoServer;
                talkerOrder = TALKER_Mode_TalktoServer;
                xQueueSend(qTalker[BOSS_qOrder], &talkerOrder, 2);
            }
            else
            {
                talkerOrder = TALKER_Mode_ConnectWifi;
                xQueueSend(qTalker[BOSS_qOrder], &talkerOrder, 2);
            }
        break;
        case BOSS_Mode_TalkingtoServer:
            // handle button
            xQueueReceive(qChecker[BOSS_qReceive], &checkerData, 2);
            xQueueReceive(qTalker[BOSS_qReceive], &talkerData, 2);
            lightData = BOSS_ButtonHandler(checkerData, talkerData, lightStatus);

            if( lightData == BUTTON_HOLD3 )
            {
                bossMode = BOSS_Mode_GetWifiInfor;
                break;
            }
            
            if(checkerData != 0)
                xQueueSend(qTalker[BOSS_qSend], &lightData, 2);
        break;
        case BOSS_Mode_GetWifiInfor:
            xQueueReceive(qTalker[BOSS_qReceive], &talkerTeller, 2);

            if(talkerTeller != TALKER_Tell_GotWifiInfor)
            {
                talkerOrder = TALKER_Mode_GetWifiInfor;
                xQueueSend(qTalker[BOSS_qOrder], &talkerOrder, 2);
            }
            else
                bossMode = BOSS_Mode_SetIDLE;
        break;
        }

        vTaskDelay(1);
    }
}

void ConfigLight(void)
{
    ///*
    // select pins function
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO4_U, FUNC_GPIO4);
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_GPIO5_U, FUNC_GPIO5);
    gpio16_output_conf();
    //*/

    // set all used pin to 0
    GPIO_OUTPUT_SET(BOSS_Ctr1, false);
    GPIO_OUTPUT_SET(BOSS_Ctr2, false);
    gpio16_output_set(false);
}

uint8 BOSS_ButtonHandler(uint8 checker, uint8 talker, bool* light)
{
    uint8 ret = 0;

    if(checker == 0)
    {
        // no request from checker
        switch(talker)
        {
        case NO_BUTTON:
            light[LIGHT1] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 0;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1:
            light[LIGHT1] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 0;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON2:
            light[LIGHT1] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 0;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON3:
            light[LIGHT1] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 1;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON2:
            light[LIGHT1] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 0;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON3:
            light[LIGHT1] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 1;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON2 | BUTTON3:
            light[LIGHT1] = 0;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 1;
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON2 | BUTTON3:
            light[LIGHT1] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
            light[LIGHT2] = 1;
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
            light[LIGHT3] = 1;
            gpio16_output_set(light[LIGHT3]);
        break;
        /*
        case BUTTON1:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
        break;
        case BUTTON2:
            light[LIGHT2] = !light[LIGHT2];
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
        break;
        case BUTTON3:
            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON2:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);

            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON3:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);

            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON2 | BUTTON3:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);

            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON1 | BUTTON2 | BUTTON3:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);

            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        */
        }
    }
    else
    {
        // have request from checker
        switch(checker)
        {
        case BUTTON1:
            light[LIGHT1] = !light[LIGHT1];
            GPIO_OUTPUT_SET(BOSS_Ctr1, light[LIGHT1]);
        break;
        case BUTTON2:
            light[LIGHT2] = !light[LIGHT2];
            GPIO_OUTPUT_SET(BOSS_Ctr2, light[LIGHT2]);
        break;
        case BUTTON3:
            light[LIGHT3] = !light[LIGHT3];
            gpio16_output_set(light[LIGHT3]);
        break;
        case BUTTON_HOLD1:
            
        break;
        case BUTTON_HOLD2:
            
        break;
        case BUTTON_HOLD3:
            ret = BUTTON_HOLD3;
        break;
        }
    }

    if(checker <= 0x0f)
        ret = light[0] + light[1]*10 + light[2]*100;        

    return ret;
}

