#include "connection.h"

bool connect_wifi_AP(const char *AP_SSID, const char *AP_PASSWORD)
{
    if(!wifi_set_opmode_current(STATION_MODE))
        return false;

    struct station_config* config = (struct station_config*)calloc(1, sizeof(struct station_config));
    sprintf(config->ssid, AP_SSID);
    sprintf(config->password, AP_PASSWORD);

    wifi_station_set_config(config);
    free(config);

    wifi_station_connect();

    return true;
}

int create_tcp_connection(struct sockaddr_in *SERVER, const char *IP, const int PORT)
{
    // create socket
    int status, socket_des;

    socket_des = socket(AF_INET, SOCK_STREAM, 0);
    if (socket_des == -1)
    {
        printf("Create socket error!!!\n");
        close(socket_des);
        return -1;
    }

    printf("Socket created\n");
    
    // create tcp connection
    bzero(SERVER, sizeof(struct sockaddr_in));
    SERVER->sin_addr.s_addr = inet_addr(IP);
    SERVER->sin_family = AF_INET;
    SERVER->sin_port = htons(PORT);

    status = connect(socket_des, (struct sockaddr *)  SERVER,  sizeof(struct sockaddr));

    if (status != 0)
    {
        printf("Connect error: %d\n", status);
        close(socket_des);
        return -2;
    }

    return socket_des;
}

bool generate_wifi(const char* SSID, const char* PASSWORD)
{
    //wifi_set_opmode(NULL_MODE);
    if (!wifi_set_opmode(SOFTAP_MODE))
        return false;

    // config upcomming generated wifi
    struct softap_config *config = (struct softap_config *)calloc(1, sizeof(struct softap_config) );
    wifi_softap_get_config(config);
    sprintf(config->ssid, SSID);
    sprintf(config->password, PASSWORD);
    config->authmode = AUTH_WPA_WPA2_PSK;
    config->ssid_len = 0;
    config->max_connection = 4;
    if (!wifi_softap_set_config(config))
        return false;
    free(config);

    //wifi_softap_dhcps_stop();
    //wifi_softap_dhcps_start();

    return true;
}

void init_server(struct sockaddr_in *server, int *sock, int PORT)
{
    bool ret = false;

    //bzero(server, sizeof(struct sockaddr_in));
    memset(server, 0, sizeof(*server));
    server->sin_family = AF_INET;
    server->sin_addr.s_addr = INADDR_ANY;
    server->sin_len = sizeof(*server);
    server->sin_port = htons(PORT);

    // create socket for incoming connections
    do
    {
        *sock = socket(AF_INET, SOCK_STREAM, 0);
        if(*sock == -1)
        {
            printf("ESP8266 TCP server task > socket error\n");
            vTaskDelay(100);
        }
    } while(*sock == -1);

    printf("ESP8266 TCP server task > create socket\n");

    // bind to PORT
    do
    {
        ret = bind(*sock, (struct sockaddr *)server, sizeof(*server));

        if(ret != 0)
        {
            printf("ESP8266 TCP server task > bind fail\n");
            vTaskDelay(100);
        }
    } while(ret != 0);

    printf("ESP8266 TCP server task > port:%d\n",ntohs(server->sin_port));

    do
    {
        ret = listen(*sock, 2);
        if(ret != 0)
        {
            printf("ESP8266 TCP server task > failed to set listen queue!\n");
            vTaskDelay(100);
        }
    } while(ret != 0);

    printf("ESP8266 TCP server task > listen ok\n");
}

char* ReadLine(int SOCKET_DES)
{
    int checker = true;
    char* recv_buf = (char *)calloc( 11, sizeof(char) );

    while (1)
    {
        char *recv_cha;
        recv_cha = (char *)calloc( 2, sizeof(char) );
        checker = read( SOCKET_DES, recv_cha, sizeof(char) );
        //recv_cha[checker] = 0;

        if (!checker)
            break;

        if (recv_cha[0] == '\n')
        {
            free(recv_cha);
            break;
        }

        strcat(recv_buf, recv_cha);

        free(recv_cha);
    }

    switch (checker)
    {
    case -1:
        printf("Error\n");
        break;
    case 0:
        printf("End of file or connection closed\n");
        break;
    case 1:
        printf("Read complete!\n");
        break;
    default:
        printf("Don't know!\n");
        break;
    }

    return recv_buf;
}

int WriteLine(int SOCKET_DES, char *write_buf)
{
    char *write;
    write = (char *)calloc(strlen(write_buf) + 1, sizeof(char));
    strcat(write, write_buf);
    strcat(write, "\n");
    
    if ( write(SOCKET_DES, write, strlen(write) ) < 0 )
    {
        close(SOCKET_DES);
        vTaskDelay(1000 / portTICK_RATE_MS);
        printf("ESP8266 TCP client task > send fail\n");
        return 1;
    }
    /*
    if ( write(SOCKET_DES, "\n", strlen(write_buf) ) < 0 )
    {
        close(SOCKET_DES);
        vTaskDelay(1000 / portTICK_RATE_MS);
        printf("ESP8266 TCP client task > send fail\n");
        return 2;
    }
    */

    free(write);
    printf("ESP8266 TCP client task > send success\n");

    return 0;
}

