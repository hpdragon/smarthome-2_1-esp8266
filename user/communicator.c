#include "communicator.h"

//#include "ssl_compat-1.0.h"
//#include "esp_common.h"
#include "cJSON.h"

#include "nopoll_conn.h"
#include "nopoll_private.h"

#include "boss_define.h"
#include "connection.h"
#include "protocol.h"

/**
 * COMMUNICATOR task priority
 */
#define COMMUNICATOR_Priority       0

xTaskHandle COMMUNICATOR_Init(void *pArg)
{
    xTaskHandle communicator;

    xTaskCreate( COMMUNICATOR_Task, "COMMUNICATOR_Task", configMINIMAL_STACK_SIZE * 6, pArg, tskIDLE_PRIORITY + COMMUNICATOR_Priority, &communicator);

    return communicator;
}

static void COMMUNICATOR_Task(void *pArg)
{
    COMMUNICATORPara* comPara = (COMMUNICATORPara*)pArg;

    uint8 comMode = COMMUNICATOR_Mode_IDLE;
    uint8 talkerOrder = COMMUNICATOR_ComtoServer_IDLE;
    uint8 lightData = 0, oldlightData = 0, lightCommand = 0xff;

    uint32 ComtoServer_counter = 0;
    int32 ComtoServer_subValue = 0;

    uint32 PingtoServer_counter = 0;
    bool PingtoServer_waitPong = false;

    // variable for communicate with server
    noPollConn* conn;
    bool isConnect = false;

    // fd_set set;

    while(1)
    {
        switch(comMode)
        {
        case COMMUNICATOR_Mode_IDLE:
            comMode = COMMUNICATOR_Mode_ConnecttoServer;

            if( isConnect )
            {
                nopoll_conn_close(conn);
                isConnect = false;
            }

            vTaskDelay(10);
        break;
        case COMMUNICATOR_Mode_ConnecttoServer:
            if(COMMUNICATOR_WS_LogintoServer(&conn))
            {
                printf("COMMUNICATOR: connected to server\n");
                ComtoServer_counter = xTaskGetTickCount();
                isConnect = true;
                comMode = COMMUNICATOR_Mode_ComtoServer;
            }

        break;
        case COMMUNICATOR_Mode_ComtoServer:
            xQueueReceive(comPara->qCom[COMMUNICATOR_TALKER_qReceive], &lightData, 2);

            if(lightData != oldlightData)
            {
                printf("COMMUNICATOR: send light data to server\n");
                COMMUNICATOR_WS_SendLight(conn, lightData);

                oldlightData = lightData;
            }

            lightCommand = COMMUNICATOR_WS_GetServerCommand(conn);

            xQueueSend(comPara->qCom[COMMUNICATOR_TALKER_qSend], &lightCommand, 2);

            ComtoServer_subValue = xTaskGetTickCount() - ComtoServer_counter;
            if( ComtoServer_subValue > COMMUNICATOR_TimetoPing || ComtoServer_subValue < 0)
                comMode = COMMUNICATOR_Mode_PingtoServer;

        break;
        case COMMUNICATOR_Mode_PingtoServer:
            if( !PingtoServer_waitPong )
            {
                if( COMMUNICATOR_WS_SendPing(conn) )
                {
                    PingtoServer_waitPong = true;
                    PingtoServer_counter = 0;
                }
                else
                    comMode = COMMUNICATOR_Mode_IDLE;
            }
            else
            {
                if( PingtoServer_counter < COMMUNICATOR_TimewaitPong)
                {
                    if( COMMUNICATOR_WS_WaitPong(conn) )
                    {
                        PingtoServer_waitPong = false;
                        ComtoServer_counter = xTaskGetTickCount();
                        comMode = COMMUNICATOR_Mode_ComtoServer;
                    }
                    else
                        PingtoServer_counter++;
                }
                else
                {
                    PingtoServer_waitPong = false;
                    comMode = COMMUNICATOR_Mode_IDLE;
                }
            }

        break;
        }

        vTaskDelay(10);
    }
}

char* COMMUNICATOR_WS_InitMessages_Login(void)
{
    uint8       macArray[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    char        macAddress[17];
    int messageLen = strlen(COMMUNICATOR_Message_Login);
    char* message = NULL;
    int i = 0;

    message = (char*)calloc(messageLen, sizeof(char));

    strcpy(message, COMMUNICATOR_Message_Login);

    wifi_get_macaddr(STATION_IF, macArray);
    sprintf(macAddress, "%02x:%02x:%02x:%02x:%02x:%02x", macArray[5], macArray[4], macArray[3], macArray[2], macArray[1], macArray[0]);

    for(i = 0; i < 17; i++)
        message[34 + i] = macAddress[i];

    for(i = 0; i < 17; i++)
        message[63 + i] = macAddress[i]; /*-1*/

    for(i = 0; i < 17; i++)
        message[92 + i] = macAddress[i]; /*-1*/

    return message;
}

char* COMMUNICATOR_WS_InitMessages_Setstt(uint8 light)
{
    uint8       macArray[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    char        macAddress[17];
    int messageLen = strlen(COMMUNICATOR_Message_SetStatus);
    char* message = NULL;
    int i = 0;

    message = (char*)calloc(messageLen, sizeof(char));

    strcpy(message, COMMUNICATOR_Message_SetStatus);

    wifi_get_macaddr(STATION_IF, macArray);
    sprintf(macAddress, "%02x:%02x:%02x:%02x:%02x:%02x", macArray[5], macArray[4], macArray[3], macArray[2], macArray[1], macArray[0]);

    for(i = 0; i < 17; i++)
        message[32 + i] = macAddress[i];

    for(i = 0; i < 17; i++)
        message[69 + i] = macAddress[i];

    for(i = 0; i < 17; i++)
        message[106 + i] = macAddress[i];

    message[59] = ((light / 1) % 10) + '0';
    message[96] = ((light / 10) % 10) + '0';
    message[133] = ((light / 100) % 10) + '0';

    return message;
}

bool COMMUNICATOR_WS_SendPing(noPollConn* conn)
{
    char*       message = NULL;
    int         messageLen = 0;

    if (! nopoll_conn_is_ready (conn))
        return false;

    messageLen = strlen(COMMUNICATOR_Message_Ping);

    message = (char*)calloc(messageLen, sizeof(char));
    strcpy(message, COMMUNICATOR_Message_Ping);

    if (nopoll_conn_send_text (conn, message, messageLen) <= 0)
        return false;

    free(message);

    return true;
}

bool COMMUNICATOR_WS_WaitPong(noPollConn* conn)
{
    noPollMsg* msg;
    char* readData;

    if ( !nopoll_conn_is_ready (conn) )
        return false;

    if( COMMUNICATOR_WS_CheckWSMailBox(conn->session) )
    {
        msg = nopoll_conn_get_msg(conn);
        if(msg == NULL)
            return false;

        readData = (char*)nopoll_msg_get_payload(msg);

        // "_pong"
        if (strncmp(readData, "_pong", 5) != 0)
        {
            free(readData);
            free(msg);
            return false;
        }
        free(readData);
        free(msg);

        return true;
    }

    return false;
}

bool COMMUNICATOR_WS_CheckWSMailBox(NOPOLL_SOCKET socket)
{
    struct timeval tv;
    fd_set readfds;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&readfds);
    FD_SET(socket, &readfds);
    FD_SET(STDIN_FILENO, &readfds);

    select(socket+1, &readfds, NULL, (fd_set*)0, &tv);

    return FD_ISSET(socket, &readfds);
}

bool COMMUNICATOR_WS_LogintoServer(noPollConn** conn)
{
    noPollCtx*  ctx;
    noPollConnOpts * opts;
    noPollMsg* msg;
    char        buffer[3];
    char* message = NULL;
    int messageLen = 0;

    message = COMMUNICATOR_WS_InitMessages_Login();

    messageLen = strlen(COMMUNICATOR_Message_Login);

    ctx = (noPollCtx *)create_ctx();
#ifdef SSL_DEMO_MODE
    opts = nopoll_conn_opts_new ();
    nopoll_conn_opts_ssl_peer_verify (opts, nopoll_false);
#endif /* SSL_DEMOE_MODE */

#ifdef SERVER_MODE
    *conn = (noPollConn*)nopoll_conn_new (ctx, "42.116.11.23", "8082", NULL, "/ws_home/dev_agents", NULL, NULL);
#endif /* SERVER_MODE */
#ifdef ECHO_MODE
    *conn = (noPollConn*)nopoll_conn_new (ctx, "echo.websocket.org", "80", NULL, NULL, NULL, NULL);
#endif /* ECHO_MODE */

#ifdef SSL_DEMO_MODE
    *conn = (noPollConn*)nopoll_conn_tls_new (ctx, opts, "192.168.100.4", "8443", NULL, "/ssl", NULL, NULL);
#endif /* SSL_DEMOE_MODE */

    if (! nopoll_conn_is_ok(*conn))
        return false;

#ifdef SERVER_MODE
        if (nopoll_conn_send_text (*conn, message, messageLen) <= 0)
#else
    if (nopoll_conn_send_text (*conn, "_ok", 3) <= 0)
#endif /* SERVER_MODE */
        return false;

    nopoll_conn_read (*conn, buffer, 3, nopoll_false, 5);

    if (strncmp(buffer, "_ok", 3) != 0)
        return false;

    free(message);

    return true;
}

uint8 COMMUNICATOR_TalktoServer_Socket(int socket_des, int8 lightData)
{
    static struct sockaddr_in server;
    static int counter = 0;
    uint8 receiveData;

    printf("%d\n", ++counter);

    printf("Creating and connecting socket...\n");

    socket_des = create_tcp_connection(&server, SERVER_IP, SERVER_PORT);

    if(socket_des < 0)
    {
        close(socket_des);
        return -1;
    }
    printf("Ready...\n");

    receiveData = Dancing_Expert(socket_des, lightData);

    printf("Data is updated to cloud!\n");

    vTaskDelay(50);

    return receiveData;
}

uint8 COMMUNICATOR_WS_SendLight(noPollConn* conn, uint8 light)
{
    char        buffer[12];
    char*       message = NULL;
    int         bytes_read, messageLen = 0;

    message = COMMUNICATOR_WS_InitMessages_Setstt(light);
    messageLen = strlen(COMMUNICATOR_Message_SetStatus);
//printf("%s\n", message);
    if (! nopoll_conn_is_ready (conn))
        return nopoll_false;

    if (nopoll_conn_send_text (conn, message, messageLen) <= 0)
        return nopoll_false;

    bytes_read = nopoll_conn_read (conn, buffer, 3, nopoll_false, 10);
    if (bytes_read != 3)
        return nopoll_false;

    /*
    nopoll_conn_close (conn);
    
    nopoll_ctx_unref (ctx);
    */

    free(message);

    return nopoll_true;
}

uint8 COMMUNICATOR_WS_GetServerCommand(noPollConn* conn)
{
    noPollMsg* msg;
    char* readData;
    uint8 command = 0xff;

    if ( !nopoll_conn_is_ready (conn) )
        return 0xff;

    if( COMMUNICATOR_WS_CheckWSMailBox(conn->session) )
    {
        msg = nopoll_conn_get_msg(conn);
        if(msg == NULL)
            return 0xff;

        printf("COMMUNICATOR: have message\n");
        readData = (char*)nopoll_msg_get_payload(msg);
        printf("%s\n", readData);

        command = readData[10] - '0';
        if(command > 1)
            return 0xff;
        command |= (readData[14] - '0') << 1;
        if(command > 11)
            return 0xff;
        command |= (readData[18] - '0') << 2;
        if(command > 111)
            return 0xff;

        free(readData);

        return command;
    }

    return 0xff;
}

