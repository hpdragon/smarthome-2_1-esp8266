#include "dealer.h"

#include "connection.h"

/**
 * Server of device runs in two mode: socket or websocket
 */
//#define DEALER_MODE_SOCKET    1
#define DEALER_MODE_WS      1

/**
 * DEALER Task priority
 */
#define DEALER_Priority     0

xTaskHandle DEALER_Init(void *pArg)
{
    xTaskHandle dealer;

    xTaskCreate( DEALER_Task, "DEALER_Task", configMINIMAL_STACK_SIZE * 3, pArg, tskIDLE_PRIORITY + DEALER_Priority, &dealer);

    return dealer;
}

void DEALER_Task(void *pArg)
{
    DEALERPara* dealerPara = (DEALERPara *)pArg;

    uint8 dealerMode = DEALER_Mode_IDLE, dealerTell = DEALER_Tell_Nothing;

#ifdef DEALER_MODE_WS
    noPollConn* listener;
    noPollCtx*  ctx;
#endif

    dealerTell = DEALER_Tell_Nothing;
    xQueueSend(dealerPara->qSend, &dealerTell, 2);

    while(1)
    {
        switch (dealerMode)
        {
        case DEALER_Mode_IDLE:
            dealerMode = DEALER_Mode_GenerateWifi;
        break;
        case DEALER_Mode_GenerateWifi:
            if( DEALER_GenerateWifi(DEALER_ESP_DefaulMyAP_SSID, DEALER_ESP_DefaulMyAP_PASSWORD) )
                dealerMode = DEALER_Mode_InitServer;
        break;
        case DEALER_Mode_InitServer:
        #ifdef DEALER_MODE_SOCKET
            dealerPara->espSocket = DEALER_InitServer();
            if(dealerPara->espSocket != -1)
                dealerMode = DEALER_Mode_GetWifiInfor;
        #endif

        #ifdef DEALER_MODE_WS
            printf("xxxxxx\n");
            if( DEALER_WS_InitServer(&listener, &ctx) )
                dealerMode = DEALER_Mode_GetWifiInfor;
        #endif
        break;
        case DEALER_Mode_GetWifiInfor:
        #ifdef DEALER_MODE_SOCKET
            if( DEALER_GetWifi(dealerPara->espSocket, dealerPara->ssid, dealerPara->password) == true )
            {
                printf("DEALER: got wifi infor\n");
                dealerTell = DEALER_Tell_GotWifiInfor;
                xQueueSend(dealerPara->qSend, &dealerTell, 2);
            }
        #endif

        #ifdef DEALER_MODE_WS
            //printf("Websocket: getting wifi infor\n");
            DEALER_WS_GetWifi(listener, ctx, dealerPara->ssid, dealerPara->password);
        #endif
        break;
        }

        vTaskDelay(50);
    }
}

bool DEALER_GenerateWifi(const char *MY_AP_SSID, const char *MY_AP_PASSWORD)
{
    wifi_station_disconnect();

    if(generate_wifi(MY_AP_SSID, MY_AP_PASSWORD))
        return true;
    else
        return false;
}

int DEALER_InitServer(void)
{
    struct sockaddr_in espServer;
    int espSocket;

    init_server(&espServer, &espSocket, DEALER_ESP_ServerPort);

    return espSocket;
}

bool DEALER_GetWifi(int espSocket, char *AP_SSID, char *AP_PASSWORD)
{
    struct sockaddr_in remote_addr;
    int32 client_sock;
    int32 len = sizeof(struct sockaddr_in);
    //-char receiveData[10];

    if((client_sock = accept(espSocket, (struct sockaddr *)&remote_addr, (socklen_t *)&len)) < 0)
    {
        printf("ESP8266 TCP server task > accept fail\n");
    }
    printf("ESP8266 TCP server task > wait client\n");

    printf("New client connected\n");

    char* read = ReadLine(client_sock);
    //-strcpy(receiveData, read);
    //printf("-");
    printf("%s\n", read);
    if( strcmp(read, DEALER_ESP_LoginId) )
    {
        printf("not\n");
        free(read);
        close(client_sock);
        return false;
    }
    free(read);

    printf("Recoding new wifi\n");
    // ssid
    read = ReadLine(client_sock);
    if(strlen(read) > 0)
    {
        //free(DEMO_AP_SSID);
        strcpy( AP_SSID, read);
        free(read);
    }
    else
    {
        free(read);
        close(client_sock);
        return false;
    }

    // password
    read = ReadLine(client_sock);
    if(strlen(read) > 0)
    {
        //free(DEMO_AP_PASSWORD);
        strcpy( AP_PASSWORD, read);
        free(read);
    }
    else
    {
        free(read);
        close(client_sock);
        return false;
    }

    printf("%s\n%s\n", AP_SSID, AP_PASSWORD);
    //free(receiveData);
    close(client_sock);

    vTaskDelay(100);

    return true;
}

//--------------------------------------------------------------------//
// Websocket

bool DEALER_WS_CheckWSMailBox(NOPOLL_SOCKET socket)
{
    struct timeval tv;
    fd_set readfds;

    tv.tv_sec = 0;
    tv.tv_usec = 0;

    FD_ZERO(&readfds);
    FD_SET(socket, &readfds);
    FD_SET(STDIN_FILENO, &readfds);

    select(socket+1, &readfds, NULL, (fd_set*)0, &tv);

    return FD_ISSET(socket, &readfds);
}

bool DEALER_WS_InitServer(noPollConn** listener, noPollCtx** ctx)
{
    //noPollCtx*  ctx;

    *ctx = (noPollCtx *)create_ctx();
    *listener = (noPollConn*)nopoll_listener_new(*ctx, DEALER_ESP_WS_Address, DEALER_ESP_WS_ServerPort);
    //free(ctx);
    
    if (! nopoll_conn_is_ok(*listener))
    {
        nopoll_conn_close(*listener);
        return false;
    }
    
    nopoll_ctx_set_on_msg(*ctx, DEALER_WS_ListeneronMessage, NULL);

    //nopoll_loop_wait (*ctx, 10000);

    return true;
}

void DEALER_WS_ListeneronMessage(noPollCtx* ctx, noPollConn* conn, noPollMsg* msg, noPollPtr user_data)
{
    char* receiveData;
printf("DEALER: on Message\n");

    // print the message (for debugging purposes) and reply
    /*
    printf ("Listener received (size: %d, ctx refs: %d): (first %d bytes, fragment: %d) '%s'\n", 
    nopoll_msg_get_payload_size (msg),
    nopoll_ctx_ref_count (ctx), shown, nopoll_msg_is_fragment (msg), example);
    */

    receiveData = (char*)nopoll_msg_get_payload(msg);
    printf("%s\n", receiveData);

    // reply to the message
    nopoll_conn_send_text (conn, "Message received", 16);

    return;
}

bool DEALER_WS_GetWifi(noPollConn* listener, noPollCtx* ctx, char *AP_SSID, char *AP_PASSWORD)
{
    //if( nopoll_listener_accept (listener->session) )
    //  printf("---------------------------\n");
    
    char buffer[10];
    
    noPollConn* conn = nopoll_conn_accept(ctx, listener);
    
    nopoll_conn_read (conn, buffer, 10, nopoll_false, 10000);
    printf("----------------------\n");
    
    //nopoll_conn_send_text (conn, "Message received", 16);

    //printf("DEALER: getting wifi\n");

    //nopoll_ctx_set_on_msg(ctx, DEALER_WS_ListeneronMessage, NULL);

    //nopoll_loop_wait (ctx, 100000);

    /*
    noPollMsg* msg;
    char* readData;

    if( !COMMUNICATOR_WS_CheckWSMailBox(listener->session) )
    {
printf("a\n");
        return false;
    }

    msg = nopoll_conn_get_msg(listener);
    if(msg == NULL)
    {
printf("b\n");
        return false;
    }
printf("c\n");
    readData = (char*)nopoll_msg_get_payload(msg);

    printf("%s\n", readData);
    if( strcmp(readData, DEALER_ESP_LoginId) )
    {
        printf("not\n");
        free(readData);
        nopoll_conn_close(listener);
        return false;
    }
    free(readData);
    */

    /*
    printf("New client connected\n");

    char* read = ReadLine(client_sock);
    //-strcpy(receiveData, read);
    //printf("-");
    printf("%s\n", read);
    if( strcmp(read, DEALER_ESP_LoginId) )
    {
        printf("not\n");
        free(read);
        close(client_sock);
        return false;
    }
    free(read);

    printf("Recoding new wifi\n");
    // ssid
    read = ReadLine(client_sock);
    if(strlen(read) > 0)
    {
        //free(DEMO_AP_SSID);
        strcpy( AP_SSID, read);
        free(read);
    }
    else
    {
        free(read);
        close(client_sock);
        return false;
    }

    // password
    read = ReadLine(client_sock);
    if(strlen(read) > 0)
    {
        //free(DEMO_AP_PASSWORD);
        strcpy( AP_PASSWORD, read);
        free(read);
    }
    else
    {
        free(read);
        close(client_sock);
        return false;
    }

    printf("%s\n%s\n", AP_SSID, AP_PASSWORD);
    //free(receiveData);
    close(client_sock);

    vTaskDelay(100);

    return true;
    */
}

