#include "protocol.h"

bool IN_PROTOCOL = false;

const char *CHIP_ID = "123456789";

uint8 Dancing_Expert(int SOCKET_DES, uint8 LightData)
{
    printf("---Entering expert protocol---\n");
    IN_PROTOCOL = true;
    char *write_data;
    write_data = (char *)calloc(MESSAGE_LENGTH, sizeof(char));
    char *receive_data;
    receive_data = (char *)calloc(MESSAGE_LENGTH, sizeof(char));

    // step 1
    printf("---STEP 1---\n");

    // write data
    printf("Writing data...\n");

    char *writelight;
    writelight = (char *)calloc( 3, sizeof(char) );
    sprintf(writelight, "%d", LightData);

    strcat(write_data, (char*) CHIP_ID);
    strcat(write_data, "-");
    strcat(write_data, writelight);

    WriteLine(SOCKET_DES, write_data);
    free(writelight);

    // read data from server
    printf("Reading data...\n");

    char* read = ReadLine(SOCKET_DES);
    strcat(receive_data, read);
    printf("Received data: %s\n", receive_data);
    printf("%d\n", LightData);
    free(read);

    close(SOCKET_DES);

    uint8 readNum = atoi(receive_data);
    free(receive_data);
    free(write_data);
    IN_PROTOCOL = false;
    return readNum;
}

uint8 PROTOCOL_ChattingwithServer()
{
    
}

