#include "boss_define.h"
#include "checker.h"

#include "freertos/task.h"
#include "freertos/queue.h"

#include "gpio.h"

/**
 * CHECKER task priority
 */
#define CHECKER_Priority        0

xTaskHandle CHECKER_Init(void *pArg)
{
    xTaskHandle checker;

    ConfigButton();

    xTaskCreate( CHECKER_Task, "CHECKER_Task", configMINIMAL_STACK_SIZE, pArg, tskIDLE_PRIORITY + CHECKER_Priority, checker);

    return checker;
}

void CHECKER_Task(void *pArg)
{
    xQueueHandle* qBoss = (xQueueHandle *)pArg;

    uint8 button[] = {CHECKER_Button1, CHECKER_Button2, CHECKER_Button3};
    bool firstPress[4] = {false, false, false, false};
    uint8 i = 0, buttonData = 0;
    uint32 time = 0;

    while(1)
    {
        for (i = 0; i < CHECKER_NumberofButton; i++)
        {
            if (firstPress[i])
            {
                // button i is pressed
                if ( GPIO_INPUT_GET(button[i]) )
                {
                    time = 0;
                    CHECKER_ButtonHandler(button[i], &buttonData);

                    while(GPIO_INPUT_GET(button[i]))
                    {
                        if( CHECKER_CheckButtonHold(button[i], time) == CHECKER_ButtonHold3 )
                        {
                            buttonData = BUTTON_HOLD3;
                            xQueueSend(qBoss[CHECKER_qSend], &buttonData, 2);
                            buttonData = 0;
                            time = 0;
                            continue;
                        }
                        time += 3;
                        vTaskDelay(1);
                    }
                }

                firstPress[i] = false;
            }

            firstPress[i] = GPIO_INPUT_GET(button[i]);
        }

        xQueueSend(qBoss[CHECKER_qSend], &buttonData, 2);
        buttonData = 0;
        vTaskDelay(1);
    }
}

void ConfigButton(void)
{

    ///*
    // for nodemcu
    // select pins function
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTMS_U, FUNC_GPIO14);
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTDI_U, FUNC_GPIO12);
    PIN_FUNC_SELECT(PERIPHS_IO_MUX_MTCK_U, FUNC_GPIO13);
    //*/


    /*
    // for uhome
    GPIO_ConfigTypeDef *but;
    but = (GPIO_ConfigTypeDef *)calloc(1, sizeof(GPIO_ConfigTypeDef));

    but->GPIO_Mode = GPIO_Mode_Input;
    but->GPIO_Pullup = GPIO_PullUp_EN;
    but->GPIO_IntrType = GPIO_PIN_INTR_DISABLE;

    but->GPIO_Pin = GPIO_Pin_14;
    gpio_config(but);
    but->GPIO_Pin = GPIO_Pin_12;
    gpio_config(but);
    but->GPIO_Pin = GPIO_Pin_13;

    free(but);
    */

    ///*
    GPIO_OUTPUT_SET(CHECKER_Button1, false);
    GPIO_OUTPUT_SET(CHECKER_Button2, false);
    GPIO_OUTPUT_SET(CHECKER_Button3, false);
    //*/
}

void CHECKER_ButtonHandler(uint8 but, uint8* data)
{
    switch(but)
    {
    case CHECKER_Button1:
        printf("CHECKER: Button 1 is pressed\n");
        *data |= BUTTON1;
    break;
    case CHECKER_Button2:
        printf("CHECKER: Button 2 is pressed\n");
        *data |= BUTTON2;
    break;
    case CHECKER_Button3:
        printf("CHECKER: Button 3 is pressed\n");
        *data |= BUTTON3;
    break;
    }
}

uint8 CHECKER_CheckButtonHold(uint8 but, uint32 time)
{
    uint32 ret = CHECKER_ButtonHoldNone;

    switch(but)
    {
    case CHECKER_Button1:
        
    break;
    case CHECKER_Button2:
        
    break;
    case CHECKER_Button3:
        if(time > 1000)
        {
            ret = CHECKER_ButtonHold3;
            printf("CHECKER: Button 3 is hold\n");
        }
    break;
    }

    return ret;
}

