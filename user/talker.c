#include "talker.h"

#include "connection.h"
// for now, will change later
#include "communicator.h"
#include "dealer.h"

#include "freertos/task.h"
#include "freertos/queue.h"

#define TALKER_Priority     0

/**
 * TALKER wifi information for connection to server
 */
/*
char* TALKER_DefaultWifi_SSID       =       "[UIT-CEEC] A215";
char* TALKER_DefaultWifi_PASSWORD   =       "hoicaigimahoi";
*/
///*
char* TALKER_DefaultWifi_SSID       =       "Siprit of fire";
char* TALKER_DefaultWifi_PASSWORD   =       "cotranostra";
//*/
/*
char* TALKER_DefaultWifi_SSID       =       "Hpdragon";
char* TALKER_DefaultWifi_PASSWORD   =       "aky5dpzuhyqdw";
*/

xTaskHandle TALKER_Init(void *pArg)
{
    xTaskHandle talker;

    wifi_set_event_handler_cb(wifi_handle_event_cb);

    xTaskCreate( TALKER_Task, "TALKER_Task", configMINIMAL_STACK_SIZE * 2, pArg, tskIDLE_PRIORITY + TALKER_Priority, talker);

    return talker;
}

volatile SYSTEM_EVENT wifiStatus;
void TALKER_Task(void *pArg)
{
    xQueueHandle* qBoss = (xQueueHandle *)pArg;

    uint8 talkerMode = TALKER_Mode_IDLE, talkerTell = TALKER_Tell_Nothing;
    uint8 lightData = 0, lightCommand = 0;
    bool firstTime = false, dealerStatus = false, comStatus = false;

    uint8 dealerTell = DEALER_Tell_Nothing;
    DEALERPara dealerPara;
    xTaskHandle dealerTask;

    uint8 comTell;
    COMMUNICATORPara comPara;
    xTaskHandle comTask;

    dealerPara.qSend = xQueueCreate(1, sizeof(uint8));

    comPara.qCom[TALKER_COMMUNICATOR_qSend] = xQueueCreate(1, sizeof(uint8));
    comPara.qCom[TALKER_COMMUNICATOR_qReceive] = xQueueCreate(1, sizeof(uint8));
    comPara.qCom[COMMUNICATOR_TALKER_qOrder] = xQueueCreate(1, sizeof(uint8));

    while(1)
    {
        xQueueReceive(qBoss[TALKER_qOrder], &talkerMode, 2);

        switch(talkerMode)
        {
        case TALKER_Mode_IDLE:
printf("TALKER: IDLE\n");
            firstTime = false;

            talkerTell = TALKER_Tell_Nothing;
            xQueueSend(qBoss[TALKER_qSend], &talkerTell, 2);
        break;
        case TALKER_Mode_ConnectWifi:
            // connect to wifi
            if(!firstTime)
            {
                if( TALKER_ConnectWifi(TALKER_DefaultWifi_SSID, TALKER_DefaultWifi_PASSWORD) )
                    firstTime = true;
            }
            // waiting to get IP
            else
                if(wifiStatus == EVENT_STAMODE_GOT_IP)
                {
                    talkerTell = TALKER_Tell_GotIp;
                    xQueueSend(qBoss[TALKER_qSend], &talkerTell, 2);
                    firstTime = false;
                }
        break;
        case TALKER_Mode_TalktoServer:
            if(!comStatus)
            {
                comTask = COMMUNICATOR_Init(&comPara);
                comStatus = true;
            }
            else
            {
                xQueueReceive(qBoss[TALKER_qReceive], &lightData, 2);

                xQueueSend(comPara.qCom[TALKER_COMMUNICATOR_qSend], &lightData, 2);
                xQueueReceive(comPara.qCom[TALKER_COMMUNICATOR_qReceive], &lightCommand, 2);

                xQueueSend(qBoss[TALKER_qSend], &lightCommand, 2);
            }

/*
if ( eTaskGetState(comTask) == eReady )
{
    printf("ready\n");
}
*/
        break;
        case TALKER_Mode_GetWifiInfor:
            if(!dealerStatus)
            {
                wifi_station_disconnect();

                if(comStatus)
                {
                    vTaskDelete(comTask);
                    close(comPara.socket_des);
                    comStatus = false;
                }

                dealerTask = DEALER_Init(&dealerPara);
                dealerStatus = true;
            }
            else
            {
                xQueueReceive(dealerPara.qSend, &dealerTell, 2);

                if(dealerTell == DEALER_Tell_GotWifiInfor)
                {
                    vTaskDelete(dealerTask);
                    close(dealerPara.espSocket);
                    dealerStatus = false;

                    strcpy(TALKER_DefaultWifi_SSID, dealerPara.ssid);
                    strcpy(TALKER_DefaultWifi_PASSWORD, dealerPara.password);

                    talkerTell = TALKER_Tell_GotWifiInfor;
                    xQueueSend(qBoss[TALKER_qSend], &talkerTell, 2);
                    vTaskDelay(20);
                }
            }
        break;
        }

        vTaskDelay(1);
    }
}

bool TALKER_ConnectWifi(const char *AP_SSID, const char *AP_PASSWORD)
{
    return connect_wifi_AP(AP_SSID, AP_PASSWORD);
}

void wifi_handle_event_cb(System_Event_t *evt)
{
    printf("event %x\n", evt->event_id);
    switch (evt->event_id)
    {
    case EVENT_STAMODE_CONNECTED:
        printf("EVENT_STAMODE_CONNECTED\n");
        wifiStatus = EVENT_STAMODE_CONNECTED;

    break;
    case EVENT_STAMODE_DISCONNECTED:
        printf("EVENT_STAMODE_DISCONNECTED\n");
        wifiStatus = EVENT_STAMODE_DISCONNECTED;

    break;
    case EVENT_STAMODE_AUTHMODE_CHANGE:
        printf("EVENT_STAMODE_AUTHMODE_CHANGE\n");
        wifiStatus = EVENT_STAMODE_AUTHMODE_CHANGE;

    break;
    case EVENT_STAMODE_GOT_IP:
        printf("EVENT_STAMODE_GOT_IP\n");
        wifiStatus = EVENT_STAMODE_GOT_IP;

    break;
    case EVENT_SOFTAPMODE_STACONNECTED:
        printf("EVENT_SOFTAPMODE_STACONNECTED\n");
        wifiStatus = EVENT_SOFTAPMODE_STACONNECTED;

    break;
    case EVENT_SOFTAPMODE_STADISCONNECTED:
        printf("EVENT_SOFTAPMODE_STADISCONNECTED\n");
        wifiStatus = EVENT_SOFTAPMODE_STADISCONNECTED;

    break;
    case EVENT_STAMODE_SCAN_DONE:

    break;
    case EVENT_STAMODE_DHCP_TIMEOUT:

    break;
    case EVENT_SOFTAPMODE_PROBEREQRECVED:

    break;
    default:
        printf("undefined event");
        break;
    }
}

