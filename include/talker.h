#ifndef _TALKERH_
#define _TALKERH_

/**
 * @brief   TALKER Task
 * 
 * Ver 1.0.1
 * 
 * - Manage communicate job
 * 
 * - Control: light, button, communicate
 * 
 * - Do: 
 * 
 *  + Communicate
 * 
 *  + Create, destroy the task which update/get data to/from server
 * 
 *  + Create, destroy the task which get ssid, password from hostile area
 * 
 *  + Send command from server to boss
 */

#include "esp_common.h"
#include "lwip/sockets.h"

/**
 * Each of these indicate the specific location in a queue array of BOSS task.
 * Those location is for TALKER task know where to send, receive, receive order from BOSS task.
 */
#define TALKER_qSend                        0   /*!< For TALKER task to send to BOSS task */
#define TALKER_qReceive                     1   /*!< For TALKER task to receive from BOSS task */
#define TALKER_qOrder                       2   /*!< For TALKER task to give order from BOSS task */

/**
 * Each of these indicate the specific location in a queue array of COMMUNICATOR task.
 * Those location is for TALKER task know where to send, receive, receive order from COMMUNICATOR task.
 */
#define TALKER_COMMUNICATOR_qSend           0   /*!< For TALKER task to send to COMMUNICATOR task */
#define TALKER_COMMUNICATOR_qReceive        1   /*!< For TALKER task to receive from COMMUNICATOR task */
#define TALKER_COMMUNICATOR_qOrder          2   /*!< For TALKER task to give order from COMMUNICATOR task */

/**
 * TALKER operation mode
 */
#define TALKER_Mode_IDLE                    1   /*!< IDLE mode */
#define TALKER_Mode_ConnectWifi             2   /*!< Connect to wifi mode */
#define TALKER_Mode_TalktoServer            3   /*!< Talk to server mode */
#define TALKER_Mode_GetWifiInfor            4   /*!< Get wifi information from user mode */

/**
 * TALKER define to communicate with BOSS
 */
#define TALKER_Tell_Nothing                 1   /*!< Tell nothing to BOSS */
#define TALKER_Tell_GotIp                   2   /*!< Tell BOSS that has got IP address from user AP */
#define TALKER_Tell_WifiGenerated           3   /*!< Tell BOSS that device wifi AP has been generated */
#define TALKER_Tell_GotWifiInfor            4   /*!< Tell BOSS that has got wifi information from user */

/**
 * @brief   Init things need for talker and start talker task
 * 
 * @param   pArg    Input argumment
 * 
 * @return  Task's instance
 */
xTaskHandle TALKER_Init(void *pArg);

/**
 * @brief   Communicate with server
 * 
 * To update/get data to/from server, if necessary, make esp become AP to get ssid and password for login procedure
 * 
 * @param   pArg    Input argumment
 */
static void TALKER_Task(void *pArg);

/**
 * @brief   Connect to wifi network
 * 
 * @param   AP_SSID         ssid of user wifi network
 * @param   AP_PASSWORD     password of user wifi network
 * 
 * @return  True if success
 */
bool TALKER_ConnectWifi(const char *AP_SSID, const char *AP_PASSWORD);

/**
 * @brief   Handle wifi event
 * 
 * @param   evt system event
 */
static void wifi_handle_event_cb(System_Event_t *evt);

#endif  // _TALKERH_

