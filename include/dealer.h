#ifndef _DEALERH_
#define _DEALERH_

/**
 * @brief   DEALER Task
 * 
 * Ver 1.0
 * 
 * - Have the job to communicate with user's phone, take wifi ssid and password
 * 
 * - Do: generate wifi; establish server for user's phone transmit ssid, password; get ssid, password from hostile area
 */

#include "esp_common.h"
#include "lwip/sockets.h"

#include "freertos/task.h"
#include "freertos/queue.h"

#include <nopoll/nopoll.h>
#include "nopoll_private.h"

/**
 * DEALER Task operation modes
 */
#define DEALER_Mode_IDLE                    1   /*!< IDLE mode */
#define DEALER_Mode_GenerateWifi            2   /*!< Generate wifi (turn device into an AP) mode */
#define DEALER_Mode_InitServer              3   /*!< Open a server inside device mode */
#define DEALER_Mode_GetWifiInfor            4   /*!< Get wifi information from user mode */

/**
 * DEALER define to communicate with boss
 */
#define DEALER_Tell_Nothing                 1   /*!< Tell nothing to TALKER task */
#define DEALER_Tell_GotWifiInfor            2   /*!< Tell TALKER task that DEALER has got the wifi infomation */

/**
 * TALKER wifi information for creating it own wifi
 */
#define DEALER_ESP_DefaulMyAP_SSID          "Login UHome"   /*!< Device AP ssid */
#define DEALER_ESP_DefaulMyAP_PASSWORD      "key_keeper"    /*!< Device AP password */
#define DEALER_ESP_WS_Address               "192.168.4.1"   /*!< Device websocket server ip address */
#define DEALER_ESP_WS_ServerPort            "1705"          /*!< Device websocket server port */
#define DEALER_ESP_ServerPort               1705            /*!< Device socket server port */
#define DEALER_ESP_LoginId                  "987654321"     /*!< id for secure connection */

/**
 * DEALER parameter
 */
typedef struct dealerParameter
{
    xQueueHandle qSend;     /*!< DEALER queue for comunicate */

    int espSocket;          /*!< Socket of device own server */

    char ssid[50];          /*!< ssid of user wifi network */
    char password[50];      /*!< password of user wifi network */
} DEALERPara;

/**
 * @brief   Init things for dealer task
 * 
 * @return  Task's instance
 */
xTaskHandle  DEALER_Init(void *pArg);

/**
 * @brief   DEALER Task
 * 
 * Take the job communicate with phone to get wifi ssid, password from hostile are, send complete signal to talker if completed
 * 
 * @param   pArg    Input argument
 */
static void DEALER_Task(void *pArg);

/**
 * @brief   Generate wifi for getting ssid, password from hostile area
 * 
 * @param   MY_AP_SSID      ssid of device AP
 * @param   MY_AP_PASSWORD  password of device AP
 * 
 * @return  True if success
 */
bool DEALER_GenerateWifi(const char *MY_AP_SSID, const char *MY_AP_PASSWORD);

/**
 * @brief   Init device own server
 * 
 * @return  Socket of device own server
 */
int DEALER_InitServer(void);

/**
 * @brief   Get ssid, password from hostile area
 * 
 * @param   espSocket       Socket of device server
 * @param   AP_SSID         ssid of user wifi network
 * @param   AP_PASSWORD     password of user wifi network
 * 
 * @return  True if success
 */
bool DEALER_GetWifi(int espSocket, char *AP_SSID, char *AP_PASSWORD);

//--------------------------------------------------------------------//
// Websocket

/**
 * @brief   Check if any read data available
 * 
 * @param   socket  Socket of websocket server
 * 
 * @return  True if success
 */
bool DEALER_WS_CheckWSMailBox(NOPOLL_SOCKET socket);

/**
 * @brief   Create a websocket server
 * 
 * @param   listener    Websocket server instance
 * @param   ctx         Context of websocket server instance
 * 
 * @return  True if success
 */
bool DEALER_WS_InitServer(noPollConn** listener, noPollCtx** ctx);

/**
 * @brief   Websocket server will listen for message from user
 * 
 * @param   ctx         Context of websocket server instance
 * @param   conn        Websocket server instance
 * @param   msg         Message instance from websocket server instance
 * @param   user_data   User data
 */
void DEALER_WS_ListeneronMessage (noPollCtx* ctx, noPollConn* conn, noPollMsg* msg, noPollPtr user_data);

/**
 * @brief   Get ssid, password from hostile area
 * 
 * @param   listener        Websocket server instance
 * @param   ctx             Context of websocket server instance
 * @param   AP_SSID         ssid of user wifi network
 * @param   AP_PASSWORD     password of user wifi network
 * 
 * @return  True if success
 */
bool DEALER_WS_GetWifi(noPollConn* listener, noPollCtx* ctx, char *AP_SSID, char *AP_PASSWORD);

#endif  // _DEALERH_

