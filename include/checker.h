#ifndef _CHECKERH_
#define _CHECKERH_

/**
 * @brief   CHECKER Task
 * 
 * Ver 1.0
 * 
 * - manage button checking operator
 * 
 * - do: check for button, send button information to boss
 */

#include "esp_common.h"

/**
 * Each of these indicate the specific location in a queue array of BOSS task.
 * Those location is for CHECKER task know where to send, receive, receive order from BOSS task.
 */
#define CHECKER_qSend               0   /*!< For CHECKER task to send to BOSS task */
#define CHECKER_qReceive            1   /*!< For CHECKER task to receive from BOSS task */
#define CHECKER_qOrder              2   /*!< For CHECKER task to give order from BOSS task */

/**
 * CHECKER Task button defination 
 */
#define CHECKER_NumberofButton      3   /*!< Number of button */
#define CHECKER_Button1             13  /*!< Button 1 */
#define CHECKER_Button2             12  /*!< Button 2 */
#define CHECKER_Button3             14  /*!< Button 3 */
                                              
/**
 * CHECKER Task button is hold defination
 */
#define CHECKER_ButtonHoldNone      1   /*!< None of them is hold */
#define CHECKER_ButtonHold1         2   /*!< Button 1 is hold */
#define CHECKER_ButtonHold2         3   /*!< Button 2 is hold */
#define CHECKER_ButtonHold3         4   /*!< Button 3 is hold */

/**
 * @brief   Init things need for CHECKER and start CHECKER task
 * 
 * @param   pArg    Input argument
 * 
 * @return  Task's instance
 */
xTaskHandle CHECKER_Init(void *pArg);

/**
 * @brief   Check for button and send button infor to BOSS
 * 
 * @param   pArg    Input argument
 */
static void CHECKER_Task(void *pArg);

/**
 * @brief   Config gpio to become button
 */
void ConfigButton(void);

/**
 * @brief   Take action from which button is pressed
 * 
 * @param   but     CHECKER_Button (1, 2, 3)
 * @param   data    Is the status of the buttons, will be updated when this function finished
 */
void CHECKER_ButtonHandler(uint8 but, uint8* data);

/**
 * @brief   Take action from which button is hold
 * 
 * @param   but     CHECKER_Button (1, 2, 3)
 * @param   time    Time delay to check if button is hold
 * 
 * @return  Which button is hold (CHECKER_ButtonHold)
 * 
 * @attention   This function currently only support for button 3
 */
uint8 CHECKER_CheckButtonHold(uint8 but, uint32 time);

#endif  // _CHECKERH_

