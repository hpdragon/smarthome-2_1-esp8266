#ifndef _COMMUNICATORH_
#define _COMMUNICATORH_

/**
 * @brief   COMMUNICATOR Task
 * 
 * Ver 1.0
 * 
 * Will handle communicate with server job, send light data to server and receive command from server
 */

#include "esp_common.h"
#include "lwip/sockets.h"
#include <nopoll/nopoll.h>

#include "freertos/task.h"
#include "freertos/queue.h"

/**
 * Defines for configuration
 */
//#define LOGIN_DEBUG_MODE      /*!< Login debug mode */
#define PingPong_DEBUG_MODE     /*!< Ping, pong debug mode */
#define SERVER_MODE             /*!< Server mode */
//#define SSL_DEMO_MODE         /*!< ssl demo mode */
//#define ECHO_MODE             /*!< Echo mode */

/**
 * Each of these indicate the specific location in a queue array of TALKER task.
 * Those location is for COMMUNICATOR task know where to send, receive, receive order from TALKER task.
 */
#define COMMUNICATOR_TALKER_qSend                   0   /*!< For COMMUNICATOR task to send to TALKER task */
#define COMMUNICATOR_TALKER_qReceive                1   /*!< For COMMUNICATOR task to receive from TALKER task */
#define COMMUNICATOR_TALKER_qOrder                  2   /*!< For COMMUNICATOR task to give order from TALKER task */

/**
 * COMMUNICATOR Task operation modes
 */
#define COMMUNICATOR_Mode_IDLE                      1   /*!< IDLE mode */
#define COMMUNICATOR_Mode_ConnecttoServer           2   /*!< Connect to server mode */
#define COMMUNICATOR_Mode_ComtoServer               3   /*!< Talking (communicate) to server mode */
#define COMMUNICATOR_Mode_PingtoServer              4   /*!< Ping to server mode */

/**
 * COMMUNICATOR Task communicate to server modes
 */
#define COMMUNICATOR_ComtoServer_IDLE               1   /*!< IDLE mode */
#define COMMUNICATOR_ComtoServer_SendLight          2   /*!< Send light data to server mode */

/**
 * Server ip for talker connect to
 */
//#define SERVER_IP                                 "103.74.116.44"     /*!< Server ip */
#define SERVER_IP                                   "192.168.100.5"     /*!< Server ip */
#define SERVER_PORT                                 1705                /*!< Server port */

#ifdef LOGIN_DEBUG_MODE

    #define COMMUNICATOR_Message_Login              "{\"ops\":\"handshack\",\"data\":[{\"ID\":\"panel_contact1\"},{\"ID\":\"panel_contact2\"},{\"ID\":\"panel_contact3\"}]}"

    #define COMMUNICATOR_Message_SetStatus          "{\"ops\":\"_setstt\",\"data\":[{\"ID\":\"panel_contact1\",\"stt\":1},{\"ID\":\"panel_contact2\",\"stt\":1},{\"ID\":\"panel_contact3\",\"stt\":1}]}"

#else

    #define COMMUNICATOR_Message_Login              "{\"ops\":\"handshack\",\"data\":[{\"ID\":\"00:00:00:00:00:00_1\"},{\"ID\":\"00:00:00:00:00:00_2\"},{\"ID\":\"00:00:00:00:00:00_3\"}]}"

    #define COMMUNICATOR_Message_SetStatus          "{\"ops\":\"_setstt\",\"data\":[{\"ID\":\"00:00:00:00:00:00_1\",\"stt\":1},{\"ID\":\"00:00:00:00:00:00_2\",\"stt\":1},{\"ID\":\"00:00:00:00:00:00_3\",\"stt\":1}]}"

#endif

#define COMMUNICATOR_Message_CheckStatus            "{\"ops\":\"_checkstt\"}"


#define COMMUNICATOR_Message_Ping                   "{\"ops\":\"_ping\"}"

#ifdef PingPong_DEBUG_MODE

    // send ping after 5 second, wait pong ~1 second
    #define COMMUNICATOR_TimetoPing                     500
    #define COMMUNICATOR_TimewaitPong                   100

#else

    // send ping after 2.5 minute, wait pong ~15 second
    #define COMMUNICATOR_TimetoPing                     15000
    #define COMMUNICATOR_TimewaitPong                   1500

#endif

/**
 * COMMUNICATOR parameter
 */
typedef struct communicatorParameter
{
    xQueueHandle qCom[3];   /*!< COMMUNICATOR queue for communicate */

    int socket_des;         /*!< Socket destination of COMMUNICATOR */
} COMMUNICATORPara;

/**
 * @brief   Init things need for COMMUNICATOR and start COMMUNICATOR task
 * 
 * @return  Task's instance
 */
xTaskHandle COMMUNICATOR_Init(void *pArg);

/**
 * @brief   It's job is to communicate to server
 * 
 * @param   pArg    Input argument
 */
static void COMMUNICATOR_Task(void *pArg);

/**
 * @brief   Get mac address of ESP8266 and implement to  messages login to server
 * 
 * @return  Message login
 */
char* COMMUNICATOR_WS_InitMessages_Login(void);

/**
 * @brief   Get mac address of chip and implement to  messages set status to server
 * 
 * @param   light   Light data
 * 
 * @return  Message set status
 */
char* COMMUNICATOR_WS_InitMessages_Setstt(uint8 light);

/**
 * @brief   Send ping to server
 * 
 * @param   conn    Websocket client instance
 * 
 * @return  Ping sucessful or not
 */
bool COMMUNICATOR_WS_SendPing(noPollConn* conn);

/**
 * @brief   Wait for pong
 * 
 * @param   conn    Websocket client instance
 * 
 * @return  Wait pong sucessful or not
 */
bool COMMUNICATOR_WS_WaitPong(noPollConn* conn);

/**
 * @brief   Connect to server
 * 
 * @param   NOPOLL_SOCKET   Socket of websocket client instance
 * 
 * @return  Sucessful or not
 */
bool COMMUNICATOR_WS_CheckWSMailBox(NOPOLL_SOCKET socket);

/**
 * @brief   Login to server via websocket
 * 
 * @param   conn    Pointer websocket client instance
 * 
 * @return  Sucessful or not
 */
bool COMMUNICATOR_WS_LogintoServer(noPollConn** conn);

/**
 * @brief   Communicate with server via socket
 * 
 * @param   socket_des  Socket of server
 * @param   lightData   Light data
 * 
 * @return  Sucessful or not
 */
uint8 COMMUNICATOR_TalktoServer_Socket(int socket_des, int8 lightData);

/**
 * @brief   Communicate with server via websocket
 * 
 * @param   conn    Websocket client instance
 * @param   light   Light data
 * 
 * @return  Sucessful or not
 * 
 * @code
 * curl -d '{"ops":"handshack","data":[{"ID":"mac_0"},{"ID":"mac_0"},{"ID":"mac_0"}]}' -H "Content-Type: application/json" 42.116.11.23:8082/ws_home/dev_agents
 * 
 * {"ID":"panel_contact1","stt":1}
 * 
 *     "{"
 *     "\"ID\":\"panel_contact1\","
 *     "\"stt\":\"1\""
 *     "}";
 * 
 * {"ID":"panel_contact1"}
 * {"get_stt":[{"ID":"panel_contact1"},{"ID":"panel_contact2"},{"ID":"panel_contact1"}]}
 * 000
 * 101
 * {"ops":"handshack","data":[{"ID":"mac_0"},{"ID":"mac_0"},{"ID":"mac_0"}]}
 * @endcode
 */
uint8 COMMUNICATOR_WS_SendLight(noPollConn* conn, uint8 light);

/**
 * @brief   Receive server command
 * 
 * @param   conn    Websocket client instance
 * 
 * @return  Server command
 * 
 * @code
 * _setstt_001_000_001
 * @endcode
 */
uint8 COMMUNICATOR_WS_GetServerCommand(noPollConn* conn);

#endif  // _COMMUNICATORH_

