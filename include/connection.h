#ifndef _CONNECTIONH_
#define _CONNECTIONH_

/**
 * @brief   ESP8266 network library
 * 
 * Ver 2.0
 * 
 * - Manage everything concern with networking of ESP8266
 * 
 * - Supported: manage wifi functionality (Station, Access Point), socket connection (tcp client, tcp server)
 */

#include "esp_common.h"
#include "lwip/sockets.h"

/**
 * @brief   Connect to wifi as access point
 * 
 * @param   AP_SSID     Ssid of access point
 * @param   AP_PASSWORD Password of access point
 * 
 * @return  True if success
 */
bool connect_wifi_AP(const char *AP_SSID, const char *AP_PASSWORD);

/**
 * @brief   Create a tcp connection to SERVER (IP, PORT) at socket socket_des
 * 
 * @param   SERVER  Server instance
 * @param   IP      Ip of the server
 * @param   PORT    Port of the server
 * 
 * @return  Socket that is connected if success, else return -1
 */
int create_tcp_connection(struct sockaddr_in *SERVER, const char *IP, const int PORT);

/**
 * @brief   Generate a soft wifi access point
 * 
 * @param   SSID     Ssid of access point
 * @param   PASSWORD Password of access point
 * 
 * @return  True if successed
 */
bool generate_wifi(const char* SSID, const char* PASSWORD);

/**
 * @brief   Create a server inside ESP8266
 * 
 * @param   server  Server instance
 * @param   sock    Socket that server will accept connection
 * @param   PORT    Port that server will accept connection
 */
void init_server(struct sockaddr_in *server, int *sock, int PORT);

/**
 * @brief   Read a line from SOCKET_DES buffer
 * 
 * @param   SOCKET_DES  Socket that will be read
 * 
 * @return  String of data was read from buffer
 */
char* ReadLine(int SOCKET_DES);

/**
 * @brief   Write a line to SOCKET_DES buffer
 * 
 * @param   SOCKET_DES  Socket that will be write
 * @param   write_buf   Write data
 * 
 * @note    WRITEDATA = write_buf + "\n"
 * 
 * @return  0 if success
 */
int WriteLine(int SOCKET_DES, char *write_buf);

#endif  // _CONNECTIONH_

