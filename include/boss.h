#ifndef _BOSSH_
#define _BOSSH_

/**
 * @brief   BOSS Task
 * 
 * Ver 1.0
 * 
 * - Manage everything
 * 
 * - Control: light, button, communicate
 */

#include "esp_common.h"

#include "freertos/FreeRTOSConfig.h"
#include "freertos/FreeRTOS.h"

#include "talker.h"
#include "checker.h"

/**
 * BOSS Task operation modes
 */
#define BOSS_Mode_SetIDLE                   1   /*!< IDLE mode */
#define BOSS_Mode_Startup                   2   /*!< Startup (init) mode */
#define BOSS_Mode_TalkingtoServer           3   /*!< Talking (communicate) to server mode */
#define BOSS_Mode_GetWifiInfor              4   /*!< Get wifi information from user mode */

//#define BOSS_Task_Delay       (10 / portTICK_PERIOD_MS)

/**
 * Each of these indicate the specific location in a queue array of CHECKER or TALKER tasks.
 * Those location is for BOSS task know where to send, receive, give order to CHECKER, TALKER tasks.
 */
#define BOSS_qSend                          1   /*!< For BOSS task to send to other tasks */
#define BOSS_qReceive                       0   /*!< For BOSS task to receive from other tasks */
#define BOSS_qOrder                         2   /*!< For BOSS task to give order to other tasks */

/**
 * BOSS Task button (gpio pin)
 */
#define BOSS_Ctr1                           5   /*!< GPIO 5 */
#define BOSS_Ctr2                           4   /*!< GPIO 4 */
#define BOSS_Ctr3                           16  /*!< GPIO 16 */

/**
 * @brief   Init things need for boss and start boss task
 * 
 * @return  Task's instance
 */
xTaskHandle BOSS_Init(void);

/**
 * @brief   It's job is to command or get infor and process from checker and talker
 * 
 * @param   pArg    Input argument
 */
static void BOSS_Task(void *pArg);

/**
 * @brief   Config gpio to become light
 */
void ConfigLight(void);

/**
 * @brief   Handle both command from checker and talker
 * 
 * @param   checker Checker command
 * @param   talker  Talker command
 * @param   light   Light data
 * 
 * @return  Light data that need to be cofig
 */
uint8 BOSS_ButtonHandler(uint8 checker, uint8 talker, bool* light);

#endif  // _BOSSH_

