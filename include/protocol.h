#ifndef _PROTOCOLH_
#define _PROTOCOLH_

/**
 * @brief   BOSS Task
 * 
 * Ver 2.0
 * 
 * Provide protocol for communicate with server
 * 
 * @attention   This <protocol> module may be drop soon
 */

#include "connection.h"
//#include "lightcontrol.h"

/**
 * Message length
 */
#define MESSAGE_LENGTH 11

/**
 * @brief   Protocol for communicate with server 
 * 
 * @param   SOCKET_DES  Server socket
 * @param   lightData   Data for trasmit to server
 * 
 * @return  number of received data in server
 */
uint8 Dancing_Expert(int SOCKET_DES, uint8 LightData);

/**
 * @brief   Chat protocol with server
 * 
 * @return  Something
 * 
 * @attention   This is not completed
 */
uint8 PROTOCOL_ChattingwithServer();

#endif

