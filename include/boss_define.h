#ifndef _BOSS_DEFINEH_
#define _BOSS_DEFINEH_

/**
 * BOSS Task define
 * 
 * Defination of BOSS Task for other tasks use for easy of communication
 */

/**
 * Define button's bit position
 */
#define NO_BUTTON       0x00    /*!< No button is pressed */
#define BUTTON1         0x01    /*!< Button 1 is pressed */
#define BUTTON2         0x02    /*!< Button 2 is pressed */
#define BUTTON3         0x04    /*!< Button 3 is pressed */

/**
 * Define if a button is hold
 */
#define BUTTON_HOLD1    0x10    /*!< Button 1 is hold */
#define BUTTON_HOLD2    0x20    /*!< Button 2 is hold */
#define BUTTON_HOLD3    0x40    /*!< Button 3 is hold */

/**
 * Define light position in array
 */
#define LIGHT1          0       /*!< Light 1 */
#define LIGHT2          1       /*!< Light 2 */
#define LIGHT3          2       /*!< Light 3 */

#endif  // _BOSS_DEFINEH_

